<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\UnitRumah;

class ujianController extends Controller
{
    function getUnit()
    {
        $listrumah = UnitRumah::get();
        return response()->json($listrumah);
    }

    function createUnit(request $request){
        DB::beginTransaction();
        try{
            $unit = new UnitRumah;
            $unit->kavling = $request->input('kavling');
            $unit->blok= $request->input('blok');
            $unit->nomor_rumah= $request->input('nomor_rumah');
            $unit->harga_rumah= $request->input('harga_rumah');
            $unit->luas_tanah= $request->input('luas_tanah');
            $unit->luas_bangunan= $request->input('luas_bangunan');
            $unit->customer_id= $request->input('customer_id');
            $unit->save();

            DB::commit();
            return response()->json(["message" => "success!!"], 500);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    function deleteUnit(request $request){
       DB::beginTransaction();
        try{
        $this->validate($request, ['id' => 'required']);
        $id = (integer)$request->input('id');
        $user= UnitRumah::find($id);
        if (empty($user)) {
            return response()->json(["message" => "Unit Not Found"], 500);
        }
        $user->delete();
        DB::commit();
        return response()->json(["message" => "success!!"], 500);
        }

        catch(\Exception $e){
        DB::rollBack();
        return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}

